# 聚BT - 聚合最优质的BT、磁力资源   
### 请Ctrl+D收藏本网页地址，确保永久访问  

| 地址       | 类型  | 是否需要翻墙 |  
| :---       |     :---:      |          ---: |    
| [jubt4.one](https://jubt4.one)    | 最新地址 | 不需要 |   
| [jubt12.xyz](https://jubt12.xyz)    | 最新地址 | 不需要 |   
| [jubt.top](https://jubt.top) | 永久地址 | 不需要 |  
 

永久：永久官方地址，会自动跳转到最新地址

最新：最新可访问地址，建议使用 


聚BT论坛： 

[https://bbs.jubt4.one](https://bbs.jubt4.one)  

[https://bbs.jubt12.xyz](https://bbs.jubt12.xyz) 

[https://bbs.jubt.top](https://bbs.jubt.top)   


强烈建议配置DoH/DoT，能解决大部分访问问题：[Chrome/Firefox配置DoH，解决DNS劫持问题](https://www.yeeach.com/post/1507)  


支持邮箱：[support@jubt.net](mailto:support@jubt.net)  

Twitter：[@jubtnet8](https://twitter.com/jubt8)  

地址发布页：[https://1jubt.top](https://1jubt.top)  或  [https://1jubt.vip](https://1jubt.vip)  


i学最新地址：[https://ixue.me](https://ixue.me) 


[聚BT被UC等国产浏览器拦截解决办法](https://bbs.jubt.top/thread-3120.htm)  
  






